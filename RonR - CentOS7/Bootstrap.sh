#!/usr/bin/env bash

echo Starting provisioning...

sudo su

yum install -y ruby
yum install -y patch gcc gcc-c++ make automake autoconf curl-devel openssl-devel zlib-devel httpd-devel apr-devel apr-util-devel sqlite-devel
yum install -y ruby-devel rubygems
gem install rails
gem install rake
gem update --system

yum install -y postgresql-server
postgresql-setup initdb

systemctl enable postgresql.service
date > /etc/vagrant_provisioned_at

echo Provisioning ended.