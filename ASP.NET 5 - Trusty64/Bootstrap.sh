#!/usr/bin/env bash

echo Starting provisioning...

sudo su

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 36A1D7869245C8950F966E92D8576A8BA88D21E9 # Docker
echo 'deb https://get.docker.io/ubuntu docker main' | tee /etc/apt/sources.list.d/docker.list

apt-get update

apt-get install -y vim
apt-get install -y lxc-docker

date > /etc/vagrant_provisioned_at

echo Provisioning ended.