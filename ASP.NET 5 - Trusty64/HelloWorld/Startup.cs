using Microsoft.AspNet.Builder;

namespace MyApp
{
	public class Startup
	{
		public void Configure(IApplicationBuilder app)
		{
			app.UseStaticFiles();
			app.UseWelcomePage();
		}
	}
}