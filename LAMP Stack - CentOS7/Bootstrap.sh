#!/usr/bin/env bash

echo Starting provisioning...

sudo su

rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
rpm -Uvh http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm

yum install -y docker

yum install -y mysql, mysql-server
yum --enablerepo=remi,remi-php56 install -y httpd mod_env php php-common
yum --enablerepo=remi,remi-php56 install -y php-pecl-apcu php-cli php-pear php-pdo php-oci8
#yum --enablerepo=remi,remi-php56 install -y php-pecl-memcache php-pecl-memcached
yum --enablerepo=remi,remi-php56 install -y php-mysqlnd php-pgsql php-pecl-mongo php-sqlite php-gd php-mbstring php-mcrypt php-xml

if ! [ -d /var/www/html/ ]; then
  mkdir /var/www/html/
fi

ln -fs /vagrant /var/www/html/

systemctl enable httpd.service
systemctl enable mysqld.service

date > /etc/vagrant_provisioned_at

echo Provisioning ended.