#!/usr/bin/env bash

echo Starting provisioning...

sudo su
apt-get update

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 36A1D7869245C8950F966E92D8576A8BA88D21E9 
sh -c "echo deb https://get.docker.io/ubuntu docker main /etc/apt/sources.list.d/docker.list"

apt-get install -y vim
apt-get install -y lxc-docker

apt-get install -y apache2
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y php5-mysql mysql-server mysql-client
apt-get install -y libapache2-mod-php5

if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi

/etc/init.d/apache2 restart

date > /etc/vagrant_provisioned_at

echo Provisioning ended.