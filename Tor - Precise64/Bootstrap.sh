#!/usr/bin/env bash

echo Starting provisioning...

sudo su

apt-get update

apt-get install -y vim

date > /etc/vagrant_provisioned_at

echo Provisioning ended.